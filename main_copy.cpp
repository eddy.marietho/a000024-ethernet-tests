#include "mbed.h"
#include "WIZnetInterface.h"


const char * IP_Addr    = "192.168.0.8";
const char * IP_Subnet  = "255.255.255.0";
const char * IP_Gateway = "192.168.0.1";
unsigned char MAC_Addr[6] = {0x00,0x08,0xDC,0x12,0x34,0x56};


DigitalOut led1(LED1);
DigitalOut t(PB_8);
Serial pc(USBTX, USBRX);

WIZnetInterface ethernet(SPI_MOSI, SPI_MISO, SPI_SCK, SPI_CS2, D9);

int main()
{
    //Set serial port baudrate speed: 115200
    pc.baud(115200);
    pc.printf("Start main \n");
    int ret = ethernet.init(MAC_Addr, IP_Addr, IP_Subnet, IP_Gateway);
    pc.printf("return : %d", ret);

    ret = ethernet.connect();

    wait_ms(2000);
    pc.printf("IP : %s\n", ethernet.getIPAddress());
    wait_ms(100);
    pc.printf("mac : %s\n", ethernet.getMACAddress());
    wait_ms(100);
    pc.printf("Sub : %s\n", ethernet.getNetworkMask());

    while (true) {
        //led1 = 0;
        t = 0;
        wait_us(1);
        t = 1;
        //led1 = 1;
        wait_us(1);
    }
}

